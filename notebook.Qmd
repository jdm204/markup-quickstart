---
title: The Normal Distribution in R
author: Dr Jamie D Matthews
date: 1979-01-13
---

# Setup

I like `tidyverse` 😀

```{R}
#| output: false
library(tidyverse)
```

# Normal Samples

We want 100 samples from a normal distribution with mean 0 and variance
12.

```{R}
norms <- rnorm(10000, 0, 12)
```

## Plot them

```{R}
hist(norms)
```

